define([
    "atemi/io/jsonrpc",
    "atemi/util/format",
    "atemi/util/TableSort",

    "bootstrap/Tooltip",
    "bootstrap/Modal",

    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/_base/fx",
    "dojo/_base/lang",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/on",
    "dojo/query",

    "gamestradamus/analytics/internal",
    "gamestradamus/region"

], function(
    jsonrpc,
    format,
    TableSort,

    Tooltip,
    Modal,

    array,
    event,
    fx,
    lang,
    dom,
    domClass,
    domConstruct,
    on,
    query,

    analytics,
    region
) {
    var init = function() {
        var rpc = jsonrpc('/jsonrpc');
        var table = dom.byId('play-list');
        var games = query('tbody tr.game', table);

        // Fix a bug with firefox autofilling the wrong checkboxes on reload.
        query('input.finished-game', table).forEach(function(item) {
            item.checked = false;
        });

        games.forEach(function(game) {
            var id = parseInt(game.getAttribute('data-user-account-game-id'));

            // Finished checkboxes.
            // var finished = query('input.finished-game', game)[0];
            var finished = game.cells[7].children[0].children[0].children[0];
            on(finished, 'change', function(evt) {
                rpc.request({
                    method: 'useraccountgame.update',
                    params: [
                        'id=' + id + '&select(id,finished)', {finished: finished.checked}
                    ]
                }, function(data) {
                    finished.checked = data[0].finished;

                    fx.animateProperty({
                        node: game,
                        properties: {opacity: {start: 1, end: 0}},
                        duration: 150,
                        onEnd: function() {
                            game.innerHTML = '<td colspan="8"><div class="rollup-spacer"></div></td>';
                            fx.animateProperty({
                                node: game.cells[0].children[0],
                                properties: {height: {start: 40, end: 0}},
                                duration: 150,
                                onEnd: function() {
                                    domClass.add(game, 'ignore');
                                    fix_table_stripes();
                                    tabulate();
                                }
                            }).play()
                        }
                    }).play();
                });
            });

            // Track users clicking on the play links.
            // var play = query('a', game)[0]
            var play = game.cells[2].children[0];
            on(play, 'click', function(evt) {
                analytics.trackEvent('Play', 'Launch');
            });
        });

        var tabulate = function() {
            var total_cost = 0;
            var total_hours = 0;
            var total_estimated_hours = 0;

            games.forEach(function(game) {
                if (domClass.contains(game, 'blur') || domClass.contains(game, 'ignore')) {
                    return;
                }

                var value = parseFloat(game.cells[3].getAttribute('data-cost'));
                if (!isNaN(value)) {
                    total_cost += value;
                }
                value = parseFloat(game.cells[4].getAttribute('data-hours'));
                if (!isNaN(value)) {
                    total_hours += value;
                }
                value = parseFloat(game.cells[5].getAttribute('data-hours'));
                if (!isNaN(value)) {
                    total_estimated_hours += value;
                }
            });

            if (region['symbol-position'] == 'before') {
                query('tfoot td.cost', table)[0].innerHTML =
                    region['symbol'] + region['format-dollars'](total_cost);
            } else {
                query('tfoot td.cost', table)[0].innerHTML =
                    region['format-dollars'](total_cost) + region['symbol'];
            }
            query('tfoot td.hours', table)[0].innerHTML =
                region['format-hours'](total_hours);
            query('tfoot td.estimated.hours', table)[0].innerHTML =
                region['format-hours'](total_estimated_hours);
        };
        tabulate();

        //----------------------------------------------------------------------
        var fix_table_stripes = function() {
            var index = 1;
            for (var i = 0, len = games.length, tr; i < len; ++i) {
                tr = games[i];
                if (domClass.contains(tr, 'blur') || domClass.contains(tr, 'ignore')) {
                    continue;
                }

                // Fix the positioning column.
                tr.cells[0].innerHTML = '<div>' + index + '</div>';
                
                // Fix the table striping.
                if (index % 2) {
                    domClass.add(tr, 'odd');
                } else {
                    domClass.remove(tr, 'odd');
                }
                index++;
            }
        };

        TableSort(
            table, [0, 0], [
                TableSort.parseInt,
                TableSort.textContent,
                TableSort.textContent,
                function(item) { if (item) { return parseFloat(item.getAttribute('data-cost')); } else { return 0; } },
                function(item) { if (item) { return parseFloat(item.getAttribute('data-hours')); } else { return 0; } },
                function(item) { if (item) { return parseFloat(item.getAttribute('data-hours')); } else { return 0; } },
                function(item) { if (item) { return parseFloat(query('div.rating', item)[0].getAttribute('data-rating')); } else { return 0; } }
            ],
            function(data) {
                
                // Refresh the game list with the new sort ordering.
                games = query('tr.game', table);

                // Make sure that everything is ordered properly.
                fix_table_stripes();
            }
        );

        (function() {
            /* Calculate a color index for a tag based on its name */
            var color_hash = function(s) {
                var h = 0;
                for (var i = 0, len = s.length; i < len; ++i) {
                    h += (s.charCodeAt(i) ^ 2);
                }
                return h % 8;
            };

            /**
             * Provide the filtering of the playlist. 
            **/
            var filter_playlist = function() {
                // which filters are currently in effect?
                var tag_filter_list = [];
                var untag_filter_list = [];
                var genre_filter_list = [];
                var applied_filters = false;
                query('.filter-list >.filter', 'filter-list').forEach(function(filter) {
                    if (domClass.contains(filter, 'hidden')) {
                        return;
                    }
                    applied_filters = true;

                    if (filter.getAttribute('data-tag-name')) {
                        tag_filter_list.push(filter.getAttribute('data-tag-name'));
                    }
                    if (filter.getAttribute('data-untag-name')) {
                        untag_filter_list.push(
                            filter.getAttribute('data-untag-name').split(' / ')[0]
                        );
                    }
                    if (filter.getAttribute('data-genre')) {
                        genre_filter_list.push(filter.getAttribute('data-genre'));
                    }
                });

                var rollup = query('.rollup', 'filter-list')[0];
                if (applied_filters && !domClass.contains(rollup, 'open')) {
                    domClass.add(rollup, 'open');
                    fx.animateProperty({
                        node: rollup,
                        properties: {
                            height: {start: 0, end: 34},
                            paddingBottom: {start: 0, end: 10}
                        },
                        onEnd: function() {
                            rollup.style.height = 'auto';
                        }
                    }).play(); 
                } else if(!applied_filters && domClass.contains(rollup, 'open')) {
                    fx.animateProperty({
                        node: rollup,
                        properties: {
                            height: {start: rollup.clientHeight - 10, end: 0},
                            paddingBottom: {start: 10, end: 0}
                        },
                        onEnd: function() {
                            domClass.remove(rollup, 'open');
                        }
                    }).play(); 
                }

                var games = table.tBodies[0].rows;
                var count = games.length;
                var tr, group_map, tag_map, genres, filtered;
                for (var i = 0, len = games.length; i < len; ++i) {
                    tr = games[i];
                    domClass.remove(tr, 'blur');

                    if (genre_filter_list.length) {
                        genres = query('.genre', tr.cells[1])[0];
                        if (!genres) {
                            domClass.add(tr, 'blur');
                            count--;
                        } else {
                            filtered = false;
                            for (var j = 0, l = genre_filter_list.length; j < l; ++j) {
                                if (genres.innerHTML.indexOf(genre_filter_list[j]) < 0) {
                                    domClass.add(tr, 'blur');
                                    count--;
                                    filtered = true;
                                    break;
                                }
                            }
                            if (filtered) {
                                continue;
                            }
                        }
                    }

                    if (untag_filter_list.length) {
                        group_map = {};
                        query('.tag', tr.cells[1]).forEach(function(tag) {
                            var pieces = tag.getAttribute('data-tag-name').split(' / ');
                            if (pieces.length > 1) {
                                group_map[pieces[0]] = true;
                            }
                        });
                        filtered = false;
                        for (var j = 0, l = untag_filter_list.length; j < l; ++j) {
                            if (group_map[untag_filter_list[j]]) {
                                domClass.add(tr, 'blur');
                                count--;
                                filtered = true;
                                break;
                            }
                        }
                        if (filtered) {
                            continue;
                        }
                    }

                    if (tag_filter_list.length) {
                        tag_map = {};
                        query('.tag', tr.cells[1]).forEach(function(tag) {
                            tag_map[tag.getAttribute('data-tag-name')] = true;
                        });
                        for (var j = 0, l = tag_filter_list.length; j < l; ++j) {
                            if (!tag_map[tag_filter_list[j]]) {
                                domClass.add(tr, 'blur');
                                count--;
                                break;
                            }
                        }
                    }
                }

                if (!count) {
                    domClass.add(table.tBodies[0], 'hidden');
                    domClass.remove(table.tBodies[1], 'hidden');
                } else {
                    domClass.remove(table.tBodies[0], 'hidden');
                    domClass.add(table.tBodies[1], 'hidden');
                }
            }; 

            var filter_click_handler = function(evt) {
                /**
                 * Clicking on a filter should filter the library display.
                **/
                event.stop(evt);

                var filter_list = query('.filter-list >.filter', 'filter-list');

                if (evt.currentTarget.getAttribute('data-tag-name')) {
                    filter_list.some(function(filter) {
                        if (filter.getAttribute('data-tag-name') == evt.currentTarget.getAttribute('data-tag-name')) {
                            domClass.remove(filter, 'hidden');
                            return true;
                        }
                    });
                } 

                if (evt.currentTarget.getAttribute('data-untag-name')) {
                    filter_list.some(function(filter) {
                        if (filter.getAttribute('data-untag-name') == evt.currentTarget.getAttribute('data-untag-name')) {
                            domClass.remove(filter, 'hidden');
                            return true;
                        }
                    });
                }

                if (evt.currentTarget.getAttribute('data-genre')) {
                    filter_list.some(function(filter) {
                        if (filter.getAttribute('data-genre') == evt.currentTarget.getAttribute('data-genre')) {
                            domClass.remove(filter, 'hidden');
                            return true;
                        }
                    });
                } 

                filter_playlist();
                tabulate();
                fix_table_stripes();
            };
            on(query('.tag-list .tag', table), 'click', filter_click_handler);
            on(query('.dropdown a', 'filter-column-header'), 'click', filter_click_handler);

            var remove_filter_click_handler = function(evt) {
                /**
                 * Invoked when a filter is removed.
                **/
                event.stop(evt);

                domClass.add(evt.currentTarget.parentNode, 'hidden');

                filter_playlist();
                tabulate();
                fix_table_stripes();
            };
            on(query('.filter-list >.filter >a', 'filter-list'), 'click', remove_filter_click_handler);

            /* Hook up the edit tag buttons */
            var modal = dom.byId('tag-modal');
            on(query('a.edit-tags'), 'click', function(evt) {
                event.stop(evt);
                var anchor = evt.currentTarget;

                // The modal needs to know which game its editing.
                modal.setAttribute(
                    'data-user-account-game-id',
                    anchor.getAttribute('data-user-account-game-id')
                );

                // Update the checkboxes so that they represent the games tags.
                var input_tag_list = query('input.tag', modal);
                input_tag_list.forEach(function(input) {
                    input.checked = false;
                });
                var game_tag_list = anchor.getAttribute('data-tags').split('|'); 
                array.forEach(game_tag_list, function(tag_name) {
                    input_tag_list.some(function(input) {
                        if (input.getAttribute('data-tag-name') == tag_name) {
                            input.checked = true;
                            return true;
                        }
                    });
                });
                query('h4 span', modal)[0].innerHTML = anchor.getAttribute('data-game-name');

                query(modal).modal('show');
            });

            // Save the new list of tags when the user clicks "Save Tags"
            on(query('button.primary', modal), 'click', function(evt) {
                event.stop(evt);

                var tags = null;
                var groups = [];
                var input_tag_list = query('input.tag', modal);
                input_tag_list.forEach(function(input) {
                    if (input.checked) {
                        if (tags) {
                            tags += '|' + input.getAttribute('data-tag-name');
                        } else {
                            tags = input.getAttribute('data-tag-name');
                        }
                        groups.push(input.getAttribute('data-tag-group'));
                    }
                });

                var id = modal.getAttribute('data-user-account-game-id');
                rpc.request({
                    method: 'useraccountgame.update',
                    params: ['id=' + id + '&select(id,tags)', {tags: tags}]
                }, function(data) {
                    // update the displayed tags for this game.
                    var games = table.tBodies[0].rows;
                    for (var i = 0, len = games.length; i < len; ++i) {
                        var tr = games[i];
                        if (tr.getAttribute('data-user-account-game-id') != id) {
                            continue;
                        }

                        var edit_tags_anchor = query('.edit-tags', tr)[0];
                            edit_tags_anchor.setAttribute('data-tags', data[0].tags);

                        var tag_list_container = query('.tag-list', tr)[0];
                        domConstruct.empty(tag_list_container);
                        if (data[0].tags) {
                            data[0].tags = data[0].tags.split('|');

                            array.forEach(data[0].tags, function(qualified_tag_name, index) {
                                var pieces = qualified_tag_name.split(' / ');
                                if (pieces.length < 2) {
                                    pieces.unshift('');
                                }

                                var node = domConstruct.create('a', {
                                    className: 'tag color-' + color_hash(pieces[0]),
                                    innerHTML: pieces[1]
                                });
                                node.setAttribute('data-tag-name', qualified_tag_name);
                                node.setAttribute('title', qualified_tag_name);
                                domConstruct.place(node, tag_list_container, 'last');
                                on(node, 'click', filter_click_handler);
                            });
                        }
                        break;
                    }

                    query(modal).modal('hide');
                });
            });

            // Handle adding new tags to the users list.
            on(query('form', modal), 'submit', function(evt) {
                event.stop(evt);

                var dropdown_options = query('.dropdown .options', 'filter-column-header')[0];
                var filter_list_container = query('.filter-list', 'filter-list')[0];
                var input_tag_list_container = query('.tag-list', modal)[0];
                var input_tag_list = query('input.tag', input_tag_list_container);
                var new_tag = query('input.new-tag', modal)[0];
                var new_tag_name = new_tag.value.replace(/^\s+|\s+$/g, '');

                if (!new_tag_name) {
                    // We shouldn't create empty tags.
                    return;
                }
                var at_least_one = input_tag_list.some(function(input) {
                    if (input.getAttribute('data-tag-name') == new_tag_name) {
                        return true;
                    } else {
                        return false;
                    }
                });
                if (at_least_one) {
                    // We shouldn't create duplicate tags.
                    query(new_tag).tooltip('destroy');
                    query(new_tag).tooltip({
                        html: false,
                        placement: 'left',
                        title: 'You already have a tag named "' + new_tag_name + '". Please choose another name.',
                        trigger: 'manual'
                    });
                    query(new_tag).tooltip('show');
                    return;
                }

                rpc.request({
                    method: 'useraccounttag.create',
                    params: ['', {name: new_tag_name}]
                }, function(data) {
                    query(new_tag, modal).tooltip('destroy'); 

                    /* Add the new tag to the modal list. */
                    var node = domConstruct.create('div', {
                        innerHTML: '<label><input class="tag" ' +
                            'type="checkbox" ' +
                            'data-tag-name="' + data[0].name + '" ' +
                            'data-tag-group="">' + data[0].name +
                            '</label>'
                    });
                    domClass.add(node, 'field');
                    domConstruct.place(node, input_tag_list_container, 'last');

                    /* We need a new filter for the tag in the header. */
                    var node = domConstruct.create('div', {
                        innerHTML: '<span>' + data[0].name + '</span>' + 
                            '<a title="Stop filtering by \'' + data[0].name + '">×</a>',
                        className: 'filter hidden color-0'
                    });
                    node.setAttribute('data-tag-name', data[0].name);
                    domConstruct.place(node, filter_list_container, 'last');
                    on(query('a', node), 'click', remove_filter_click_handler);

                    // We need a new filter entry on the dropdown menu.
                    var node = domConstruct.create('a', {
                        innerHTML: data[0].name
                    });
                    node.setAttribute('data-tag-name', data[0].name);
                    domConstruct.place(node, dropdown_options, 'last')
                    on(node, 'click', filter_click_handler);

                    // Clear the input field to allow for rapid adding of multiple tags.
                    new_tag.value = '';

                }, function(data) {
                    query(new_tag, modal).tooltip('destroy');

                    var field = 'name: ';
                    if (data.error.message.slice(0, field.length) == field) {
                        query(new_tag, modal).tooltip({
                            html: false,
                            placement: 'left',
                            title: data.error.message.slice(
                                field.length,
                                data.error.message.length
                            ),
                            trigger: 'manual'
                        });
                        query(new_tag, modal).tooltip('show');
                    }
                });
            });

            on(query('input.new-tag', modal), 'focus', function(evt) {
                query('input.new-tag', modal).tooltip('destroy'); 
            });
            on(modal, 'hide', function(evt) {
                query('input.new-tag', modal).tooltip('destroy');
            });

            query(modal).modal({show: false});
        })();
    };
    return init;
});
