A request has been made to add ({{ email.email }}) to Gauge profile "{{ user_account.nickname }}". We ask that you confirm this addition by clicking the link below:


{{ email.confirmation_link() }}


If you did not make this request, you may simply ignore this message.






If you do not want to receive any further communication, please click the following unsubscribe link to have this address removed from our e-mail list: 
{{ email.unsubscribe_link() }}
